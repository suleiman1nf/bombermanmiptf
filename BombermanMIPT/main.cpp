#include <iostream>
#include "Source/Windows.h"
#include "Source/FPS.h"
#include "Source/collisions.h"
#include "Source/Animation.h"
#include "cmath"
#include "Source/Gameobject.h"
#include <sstream>
using namespace std;

Windows window(Point(300,300), 600,600, "Bomberman");
string* pathsIdle = new string[1];
Image image(Point(300,300), pathsIdle[0]);
string* paths = new string[8];
string* pathsFront = new string[8];

Animation animFront(pathsFront, 8, image, true, 0.05);
Animation anim(paths, 8, image, true, 0.05);
Animation animIdle(pathsIdle, 1, image, true, 6);
Rectan rect(Point(200,200), 100,200,0, FL_WHITE, FL_GRAY);
double speedX;
double speedY;
void callback2(void*)
{
    //anim.play();
    //animFront.play();

    FPS fps;
    //while(true)
    //{
        speedX += speedX < 1 ? 0.25 : -speedX;
        speedY += speedY < 1 ? 0.25 : -speedY;
        //cout << speedX << endl;
        int roundSpeedX = 2;
        int roundSpeedY = 2;
        //cout << roundSpeedY << endl;
        Point pos1(anim.image.get_x(), anim.image.get_y());
        Point size1(anim.image.get_width(), anim.image.get_height());
        Point pos2(rect.get_x(), rect.get_y());
        Point size2(rect.get_width(), rect.get_height());
        if(window.keyboard.is_down(Keycode::left) &&
           !rect_collision(Point(pos1.x - roundSpeedX, pos1.y), size1, pos2, size2))
        {
            image.move(-roundSpeedX,0);
            if(animFront.is_play())
                animFront.stop();
            if(!anim.is_play())
                anim.play();
            if(animIdle.is_play())
                animIdle.stop();
        }
        if(window.keyboard.is_down(Keycode::right) &&
           !rect_collision(Point(pos1.x + roundSpeedX, pos1.y), size1, pos2, size2))
        {
            image.move(roundSpeedX,0);
            if(animFront.is_play())
                animFront.stop();
            if(!anim.is_play())
                anim.play();
            if(animIdle.is_play())
                animIdle.stop();
        }
        if(window.keyboard.is_down(Keycode::down) &&
           !rect_collision(Point(pos1.x, pos1.y + roundSpeedY), size1, pos2, size2))
        {
            image.move(0,roundSpeedY);
            if(!animFront.is_play())
                animFront.play();
            if(anim.is_play())
                anim.stop();
            if(animIdle.is_play())
                animIdle.stop();
        }
        if(window.keyboard.is_down(Keycode::up) &&
           !rect_collision(Point(pos1.x, pos1.y - roundSpeedY), size1, pos2, size2))
        {
            image.move(0,-roundSpeedY);
            if(!animFront.is_play())
                animFront.play();
            if(anim.is_play())
                anim.stop();
            if(animIdle.is_play())
                animIdle.stop();
        }
        if(!window.keyboard.is_down(Keycode::up)&&
           !window.keyboard.is_down(Keycode::down)&&
           !window.keyboard.is_down(Keycode::right)&&
           !window.keyboard.is_down(Keycode::left))
        {
            if(animFront.is_play())
                animFront.stop();
            if(anim.is_play())
                anim.stop();
            if(!animIdle.is_play())
                animIdle.play();
        }
        //std::cout << window.keyboard.is_down(Keycode::down) << std::endl;
        fps.begin();
        Fl::check();
        Fl::redraw();
        fps.end();
    //}
        Fl::repeat_timeout(0.01, callback2);
}

void callback1(Fl_Widget*, void*)
{
    Gameobject wall(Point(200,200), Gameobject::Wall, "cartman.png", window);
    string* paths = new string[3];
    paths[0] = "2.jpg";
    paths[1] = "Atlas1.jpg";
    paths[2] = "cartman.png";
    wall.add_animation(paths, 3, true, 0.5);
    wall.add_animation(paths, 3, true, 0.1);
    wall.play_animation(0);
    while (true)
    {
        cout << wall.get_width() << endl;
        if(window.keyboard.is_down(Keycode::down)
            && wall.is_play_animation(0))
        {
            wall.play_animation(1);
        }
        Fl::check();
        Fl::redraw();
    }
}

int main()
{
    paths[0] = "Bman_F_f00.png";
    paths[1] = "Bman_F_f01.png";
    paths[2] = "Bman_F_f02.png";
    paths[3] = "Bman_F_f03.png";
    paths[4] = "Bman_F_f04.png";
    paths[5] = "Bman_F_f05.png";
    paths[6] = "Bman_F_f06.png";
    paths[7] = "Bman_F_f07.png";
    pathsFront[0] = "FBman_F_f00.png";
    pathsFront[1] = "FBman_F_f01.png";
    pathsFront[2] = "FBman_F_f02.png";
    pathsFront[3] = "FBman_F_f03.png";
    pathsFront[4] = "FBman_F_f04.png";
    pathsFront[5] = "FBman_F_f05.png";
    pathsFront[6] = "FBman_F_f06.png";
    pathsFront[7] = "FBman_F_f07.png";
    pathsIdle[0] = "FBman_F_f00.png";
    window.attach(rect);
    window.attach(image);
    Fl::add_timeout(0.01, callback2);
    //callback2(nullptr,nullptr);
    //callback1(nullptr, nullptr);
    return Fl::run();
}
