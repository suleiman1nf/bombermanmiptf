#ifndef WINDOWS_H
#define WINDOWS_H

#include <FL/Fl_Double_Window.H>
#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include "../Source/Point.h"
#include "../Source/Primitives.h"
#include <string>
#include <vector>
#include "../Source/GUI.h"
#include "../Source/Keyboard.h"

class Windows : Fl_Double_Window
{
public:
    Windows(Point topleft1, int width, int height,
            std::string title);
    int handle(int e);
    void attach(Shape& s);
    void attach(Widget& w);
    void draw();
    Keyboard keyboard;
private:
    std::vector<Shape*> shapes;
};

#endif // WINDOWS_H
