#include "Primitives.h"
Shape::Shape(int lw, int lc, int fc)
    :line_width(lw), line_color(lc), fill_color(fc)
{

}

void Shape::add(Point p)
{
    pts.push_back(p);
}

void Shape::move(int dx, int dy)
{
    unsigned long long size = pts.size();
    for(unsigned long long i = 0; i < size; i++){
        pts[i].x += dx;
        pts[i].y += dy;
    }
}

Rectan::Rectan(Point topleft, Point bottomright,
               int lw, int lc, int fc)
    :Shape(lw, lc, fc),width{bottomright.x - topleft.x}
    ,height{bottomright.y - topleft.y}
{
    if(height <=0 || width <=0){
        throw "Rectan height or width <= 0";
    }
    add(topleft);
}

Rectan::Rectan(Point topleft, int w, int h,
               int lw, int lc, int fc)
    :Rectan(topleft,
            Point (topleft.x + w, topleft.y + h),
            lw, lc, fc)
{
    add(topleft);
}

void Rectan::draw() const{
    fl_color(line_color);
    fl_rectf(pts[0].x - line_width, pts[0].y - line_width,
            width + 2*line_width, height + 2*line_width);
    fl_color(fill_color);
    fl_rectf(pts[0].x, pts[0].y, width, height);
}

void Circle::draw() const{
    fl_line_style(FL_SOLID, line_width);
    fl_color(fill_color);
    fl_pie(pts[0].x, pts[0].y, 2 * radius - 1, 2 * radius -1,
            0,360);
    fl_color(line_color);
    fl_arc(pts[0].x, pts[0].y, 2*radius, 2*radius,0,360);
}

Circle::Circle(Point p, int r, int lw, int lc, int fc):
    radius(r), Shape(lw, lc, fc){
    add(p);
    move(-radius, -radius);
}

Text::Text(Point p, int f, int lw, int lc, std::string s)
    :font(f), content(s), Shape(lw,lc,0)
{
    add(p);
}

void Text::draw() const
{
    fl_font(font,line_width);
    fl_color(line_color);
    fl_draw(content.c_str(), pts[0].x, pts[0].y);
}

Text::Text(Point p, int lw, std::string s)
    :Text(p, default_font, lw, default_color, s)
{}

bool can_open (const std::string & s)
{
    std::ifstream fin{s};
    return (fin.is_open());
}

std::map<std::string, Suffix::Encoding> suffix_map ={
    {"jpg", Suffix::jpg},
    {"JPG", Suffix::jpg},
    {"jpeg", Suffix::jpg},
    {"JPEG", Suffix::jpg},
    {"GIF", Suffix::gif},
    {"gif", Suffix::gif},
    {"bmp", Suffix::bmp},
    {"BMP", Suffix::bmp},
    {"png", Suffix::png},
    {"PNG", Suffix::png}
};

Suffix::Encoding get_encoding (const std::string & s)
{
  std::string::const_iterator p = std::find(s.begin(), s.end(), '.');
  if (p == s.end()) return Suffix::none;  // no suffix

  std::string suf(p+1, s.end());
  return suffix_map[suf];
}


void Image::set_mask(Point pos, int ww, int hh)
{
    w = ww;
    h = hh;
    cx = pos.x;
    cy = pos.y;
}

void Image::move(int dx, int dy)
{
    Shape::move(dx,dy);
    //p->draw(pts[0].x, pts[0].y);
}

void Image::relocate(int new_x, int new_y)
{
    pts[0].x = new_x;
    pts[0].y = new_y;
}

Image::Image (Point pos, std::string s, Suffix::Encoding e)
  : Shape(0,0,0), w{ 0 }, h{ 0 }, text{ pos, 10, "" }
{
    add(pos);
    change_image(s, e);
}

void Image::change_image(std::string s, Suffix::Encoding e){

    if(!can_open(s))
    {
        text.content = "cannot open \""+s+'\"';
        p = new Bad_image(30,20);
        return;
    }

    if(e == Suffix::none)
        e = get_encoding(s);

    switch (e) {
    case Suffix::jpg:
        p = new Fl_JPEG_Image(s.c_str());
        break;
    case Suffix::gif:
        p = new Fl_GIF_Image(s.c_str());
        break;
    case Suffix::png:
        p = new Fl_PNG_Image(s.c_str());
        break;
    default:
        text.content = "unsupported file type \""+ s +'\"';
        p = new Bad_image(30,20);
        break;
    }

    w = p->w();
    h = p->h();

}


void Image::draw() const
{
    if(text.content != "")
        text.draw();
    p->draw(pts[0].x, pts[0].y);
}
