#ifndef GUI_H
#define GUI_H

#include <FL/Fl.H>
#include <FL/Fl_Button.H>
#include "../Source/Point.h"
#include <string>

class Windows;

using Callback = void(*)(Fl_Widget*, void*);

struct Widget{
    int width, height;
    std::string label;
    Point pos;
    Callback do_it;
    Widget(Point p, int w, int h,
           std::string s, Callback cb)
        :pos{p}, width{w}, height{h}, label{s}, do_it{cb}{}
    virtual void attach(Windows&) = 0;
    virtual ~Widget(){}
protected:
    Windows *own;
};

struct Button:Widget{
    Fl_Button* pw;
    Button(Point p, int w, int h, std::string s, Callback cb);
    void attach(Windows &window) override;
};

#endif // GUI_H
