#ifndef KEYBOARD_H
#define KEYBOARD_H
#include <map>
#include <string>
#include <iostream>

struct Keycode
{
    enum Key { up, down, left, right };
};

struct Key_state{
    enum State {pressed, unpressed};
};

extern std::map<int, Keycode::Key> keycode_map;
extern std::map<Keycode::Key, Key_state::State> key_state_map;

class Keyboard
{
public:
    Keyboard();
    void set_down(int k);
    void set_up(int k);
    bool is_down(Keycode::Key);
    void reset();
private:

};

#endif // KEYBOARD_H
