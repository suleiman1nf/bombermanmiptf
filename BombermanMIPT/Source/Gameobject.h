#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H
#include "../Source/Primitives.h"
#include "../Source/Animation.h"
#include <vector>

struct Movement
{
    bool left = false;
    bool right = false;
    bool up = false;
    bool down = false;
};

enum MoveSide
{
    left, right, up, down, stop
};

class Gameobject
{
public:
    enum Type
    {
        Wall, Player, Enemy, Fire, Bonus, Destr_wall, Bomb, Ground
    };
    Gameobject(Point _pos, Type _type, std::string im_path, Windows &win);


    int get_x(){return image.get_x();}
    int get_y(){return image.get_y();}
    int get_width(){return image.get_width();}
    int get_height(){return image.get_height();}
    Type get_type(){return type;}

    void set_position(Point pos);
    void move(int dx, int dy);

    void change_movement(MoveSide side);
    void update_position();
    int get_speed(){return speed;}
    void set_speed(int _speed){speed = _speed;}
    //Animations
    void add_animation(std::string* paths, int total,
                       bool isLoop, double rate = 0.1);
    void play_animation(int anim_index);
    void stop_animation(int anim_index);
    bool is_play_animation(int anim_index);
private:

    Type type;
    Point position;
    Image image;
    std::vector<Animation> animations;
    Windows &win;
    Movement movement;
    int speed = 0;
};

#endif // GAMEOBJECT_H
