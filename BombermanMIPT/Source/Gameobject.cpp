#include "Gameobject.h"

Gameobject::Gameobject(Point _pos, Type _type, std::string im_path, Windows &_win)
    :type(_type), position(_pos), image(_pos, im_path), win(_win)
{
    win.attach(image);
}

void Gameobject::add_animation(std::string* paths, int total,
                               bool isLoop, double rate)
{
    Animation anim(paths, total, image, isLoop, rate);
    animations.push_back(anim);
}

void Gameobject::set_position(Point pos)
{
    image.relocate(pos.x, pos.y);
}

void Gameobject::move(int dx, int dy)
{
    image.move(dx, dy);
}

void Gameobject::play_animation(int anim_index)
{
    if(anim_index < animations.size())
    {
        for(int i = 0; i < animations.size(); i++)
        {
            animations[i].stop();
        }
        animations[anim_index].play();

    }
    else
        std::cout << "can't play animation, wrong index";
}

void Gameobject::stop_animation(int anim_index)
{
    if(anim_index < animations.size())
        animations[anim_index].stop();
    else
        std::cout << "can't stop animation, wrong index";
}

bool Gameobject::is_play_animation(int anim_index)
{
    if(anim_index < animations.size())
        return animations[anim_index].is_play();
    else
        std::cout << "can't access animation, wrong index";
}

void Gameobject::change_movement(MoveSide side)
{
    switch (side) {
    case MoveSide::left:
        movement.left = true;
        movement.right = false;
        movement.up = false;
        movement.down = false;
        break;
    case MoveSide::right:
        movement.left = false;
        movement.right = true;
        movement.up = false;
        movement.down = false;
        break;
    case MoveSide::up:
        movement.left = false;
        movement.right = false;
        movement.up = true;
        movement.down = false;
        break;
    case MoveSide::down:
        movement.left = false;
        movement.right = false;
        movement.up = false;
        movement.down = true;
        break;
    case MoveSide::stop:
        movement.left = false;
        movement.right = false;
        movement.up = false;
        movement.down = false;
    default:
        break;
    }
}

void Gameobject::update_position()
{
    if(movement.left)
        move(-speed,0);
    if(movement.right)
        move(speed,0);
    if(movement.up)
        move(0, -speed);
    if(movement.down)
        move(0, speed);
}
