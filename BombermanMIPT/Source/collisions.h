#ifndef COLLISIONS_H
#define COLLISIONS_H
#include "../Source/Point.h"
int rect_collision(Point pos1, Point size1, Point pos2, Point size2){
    if(pos1.x < pos2.x + size2.x &&
       pos1.x + size1.x > pos2.x &&
       pos1.y < pos2.y + size2.y &&
       size1.y + pos1.y > pos2.y){
       return 1;
    }
    return 0;
}


#endif // COLLISIONS_H

