#ifndef PRIMITIVES_H
#define PRIMITIVES_H

#include <FL/Fl.H>
#include <Fl/fl_draw.H>
#include "../Source/Point.h"
#include <vector>
#include <string>
#include <iostream>
#include <map>
#include <fstream>
#include <algorithm>
#include <FL/Fl_JPEG_Image.H>
#include <FL/Fl_GIF_Image.H>
#include <FL/Fl_PNG_Image.H>

struct Shape{
public:
    std::vector<Point> pts;
    Shape(int lw, int lc, int fc);
    void add(Point p);
    virtual int get_x() const {return pts[0].x;}
    virtual int get_y() const {return pts[0].y;}
    virtual void move(int dx, int dy);
    virtual void draw() const {}
    virtual ~Shape(){}
protected:
    int line_width, line_color, fill_color;
};

struct Rectan : Shape{
    Rectan(Point topleft, Point bottomright, int lw, int lc, int fc);
    Rectan(Point topleft, int width, int height, int lw, int lc, int fc);
    void draw() const override;
    int get_width() const {return width;}
    int get_height() const {return height;}
private:
    int width;
    int height;
};

struct Circle: Shape{
    int radius;
    Circle(Point p, int r, int lw, int lc, int fc);
    void draw() const override;
};

struct Text: Shape{
    const int default_font = FL_HELVETICA;
    const int default_color = FL_RED;
    int font;
    std::string content;
    Text(Point p, int f, int lw, int lc, std::string s);
    Text(Point p, int lw, std::string s);
    void draw() const override;
};

struct Bad_image : Fl_Image
{
  Bad_image (int h, int w) : Fl_Image{ h, w, 0 }  { }

  void draw (int x, int y, int, int, int, int) override
  {
      draw_empty(x, y);
  }

};

struct Suffix
{
  enum Encoding { none, jpg, gif, bmp, png };
};

Suffix::Encoding get_encoding (const std::string & s);

extern std::map<std::string, Suffix::Encoding> suffix_map;

struct Image: Shape
{
    Image(Point pos, std::string s, Suffix::Encoding e = Suffix::none);
    ~Image(){delete p;}
    void draw() const override;
    void set_mask(Point pos, int ww, int hh);
    void move(int dx, int dy) override;
    int get_width() const {return w;}
    int get_height() const {return h;}
    void relocate(int new_x, int new_y);
    void change_image(std::string s, Suffix::Encoding e = Suffix::none);
private:
    int w,h,cx,cy;
    Fl_Image* p;
    Text text;
};



#endif // PRIMITIVES_H
