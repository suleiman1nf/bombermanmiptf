#include "Windows.h"

Windows::Windows(Point topleft, int width, int height,
                 std::string title):
        Fl_Double_Window(topleft.x, topleft.y,
                         width, height, title.c_str())
{
    color(FL_BLACK);
    show();
}

void Windows::draw(){
    Fl_Double_Window::draw();
    for(unsigned int i = 0; i < shapes.size(); i++){
        shapes[i]->draw();
    }
}

void Windows::attach(Shape& s){
    shapes.push_back(&s);
}

void Windows::attach(Widget& w){
    begin();
    w.attach(*this);
    end();
}
//Перехватываем нажатие с клавиатуры и свертывание окна
int Windows::handle(int e)
{
    int ret = Fl_Double_Window::handle(e);
    switch (e) {
    case FL_KEYDOWN:
        keyboard.set_down(Fl::event_key());
        break;
    case FL_KEYUP:
        keyboard.set_up(Fl::event_key());
        break;
    case FL_UNFOCUS:
        keyboard.reset();
        break;
    default:
        break;
    }
}
