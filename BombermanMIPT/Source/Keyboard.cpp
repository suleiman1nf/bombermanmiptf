#include "Keyboard.h"

std::map<int, Keycode::Key> keycode_map ={
    {119, Keycode::up},
    {65362, Keycode::up},
    {115, Keycode::down},
    {65364, Keycode::down},
    {97, Keycode::left},
    {65361, Keycode::left},
    {100, Keycode::right},
    {65363, Keycode::right},
};

std::map<Keycode::Key, Key_state::State> key_state_map ={
    {Keycode::up, Key_state::unpressed},
    {Keycode::down, Key_state::unpressed},
    {Keycode::left, Key_state::unpressed},
    {Keycode::right, Key_state::unpressed},
};


Keyboard::Keyboard()
{

}

void Keyboard::reset()
{
    for(auto& item : key_state_map)
    {
        item.second = Key_state::unpressed;
    }
}

void Keyboard::set_up(int k)
{
    if(keycode_map.find(k) != keycode_map.end())
        key_state_map[keycode_map[k]] = Key_state::unpressed;
    //std::cout << "keycode_map[" << k << "] = " << keycode_map[k] << std::endl;
    //std::cout << "key_state_map[" << keycode_map[k] << "] = " << key_state_map[keycode_map[k]] << std::endl;
}

void Keyboard::set_down(int k)
{
    if(keycode_map.find(k) != keycode_map.end())
        key_state_map[keycode_map[k]] = Key_state::pressed;
    //std::cout << "keycode_map[" << k << "] = " << keycode_map[k] << std::endl;
    //std::cout << "key_state_map[" << keycode_map[k] << "] = " << key_state_map[keycode_map[k]] << std::endl;
}

bool Keyboard::is_down(Keycode::Key keycode)
{
    return key_state_map[keycode] == Key_state::pressed;
}
