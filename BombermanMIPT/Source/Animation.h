#ifndef ANIMATION_H
#define ANIMATION_H
#include "../Source/Primitives.h"
#include "../Source/Windows.h"

class Animation
{
public:
    Animation(std::string* _paths, int _total, Image&, bool _isLoop, double _rate = 0.1);
    void stop();
    void play();
    int get_curr_image();
    int isLoop = false;
    void change_curr_image(int index);
    std::string* paths;
    int get_total();
    double rate;
    bool is_play();
    Image& image;
    static void show_next_image(void* d)
    {
        Animation* instance = static_cast<Animation*>(d);
        instance->image.change_image(instance->paths[instance->get_curr_image() % instance->get_total()]);
        if(!instance->isLoop && instance->get_curr_image() == instance->get_total())
            return;
        instance->change_curr_image(instance->get_curr_image() + 1);
    }
private:
    int total;
    int curr_image = 0;
    bool isPlay = false;
};

#endif // ANIMATION_H
