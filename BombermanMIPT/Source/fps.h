#ifndef FPS_H
#define FPS_H
#include <ctime>

class FPS {
private:
    clock_t deltaTime = 0;
    clock_t beginFrame = 0;
    clock_t endFrame = clock();
    unsigned int frames = 0;
    double  frameRate = 30;
    double  averageFrameTimeMilliseconds = 33.333;
    double clockToMilliseconds(clock_t ticks){
        // units/(units/time) => time (seconds) * 1000 = milliseconds
        return (ticks/(double)CLOCKS_PER_SEC)*1000.0;
    }

    void calculate(){
        deltaTime += endFrame - beginFrame;
        frames ++;
        if( clockToMilliseconds(deltaTime)>1000.0){ //every second
            frameRate = (double)frames*0.5 +  frameRate*0.5; //more stable
            frames = 0;
            deltaTime -= CLOCKS_PER_SEC;
            averageFrameTimeMilliseconds  = 1000.0/(frameRate==0?0.001:frameRate);
            std::cout<<"CPU time was:"<<averageFrameTimeMilliseconds<<std::endl;
            std::cout<<"frameRate:"<<frameRate<<std::endl;
        }
    }
public:
    void begin(){
        beginFrame = clock();
    }
    void end(){
        endFrame = clock();
        calculate();
    }
};

#endif // FPS_H
