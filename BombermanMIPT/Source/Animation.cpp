#include "Animation.h"

Animation::Animation(std::string* _paths, int _total, Image& _image, bool _isLoop, double _rate)
    :total(_total), paths(_paths), rate(_rate), image(_image), isLoop(_isLoop)
{

}

void Animation::play(){
    isPlay = true;
    change_curr_image(0);
    //Fl::add_timeout(0.01f, show_next_image, (void*)this);
}

void Animation::stop(){
    if(isPlay)
    {
        isPlay = false;
        Fl::remove_timeout(show_next_image, (void*)this);
    }
}

bool Animation::is_play(){
    return isPlay;
}

int Animation::get_curr_image()
{
    return curr_image;
}

void Animation::change_curr_image(int index)
{
    curr_image = index;
    Fl::repeat_timeout(rate, show_next_image, (void*)this);
}

int Animation::get_total()
{
    return total;
}
