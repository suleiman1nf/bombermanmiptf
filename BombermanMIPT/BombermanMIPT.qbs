import qbs

Project {
    minimumQbsVersion: "1.7.1"

    CppApplication {
        consoleApplication: true

        cpp.cxxLanguageVersion: "c++17"

        property path assets_path: "../Files"

        Group {
            name: "Runtime resources"
            files: [
                assets_path + "/Sprites/Atlas1.jpg",
                assets_path + "/Sprites/Player.jpg",
                assets_path + "/Sprites/2.jpg",
                assets_path + "/Sprites/cartman.png",
                assets_path + "/Sprites/Bomberman/Side/Bman_F_f00.png",
                assets_path + "/Sprites/Bomberman/Side/Bman_F_f01.png",
                assets_path + "/Sprites/Bomberman/Side/Bman_F_f02.png",
                assets_path + "/Sprites/Bomberman/Side/Bman_F_f03.png",
                assets_path + "/Sprites/Bomberman/Side/Bman_F_f04.png",
                assets_path + "/Sprites/Bomberman/Side/Bman_F_f05.png",
                assets_path + "/Sprites/Bomberman/Side/Bman_F_f06.png",
                assets_path + "/Sprites/Bomberman/Side/Bman_F_f07.png",
                assets_path + "/Sprites/Bomberman/Front/FBman_F_f00.png",
                assets_path + "/Sprites/Bomberman/Front/FBman_F_f01.png",
                assets_path + "/Sprites/Bomberman/Front/FBman_F_f02.png",
                assets_path + "/Sprites/Bomberman/Front/FBman_F_f03.png",
                assets_path + "/Sprites/Bomberman/Front/FBman_F_f04.png",
                assets_path + "/Sprites/Bomberman/Front/FBman_F_f05.png",
                assets_path + "/Sprites/Bomberman/Front/FBman_F_f06.png",
                assets_path + "/Sprites/Bomberman/Front/FBman_F_f07.png",
            ]
            qbs.install: true
        }

        files: [
            "Source/Animation.cpp",
            "Source/Animation.h",
            "Source/Collisions.h",
            "Source/Enemy.cpp",
            "Source/Enemy.h",
            "Source/Field.cpp",
            "Source/Field.h",
            "Source/GUI.cpp",
            "Source/GUI.h",
            "Source/Gameobject.cpp",
            "Source/Gameobject.h",
            "Source/Keyboard.cpp",
            "Source/Keyboard.h",
            "Source/Primitives.cpp",
            "Source/Primitives.h",
            "Source/Windows.cpp",
            "Source/Windows.h",
            "Source/Point.h",
            "Source/FPS.h",
            "main.cpp",
        ]

        cpp.staticLibraries: [
            "fltk_images",
            "fltk",
        ]
        Properties {
            condition: qbs.targetOS.contains("windows")
            cpp.includePaths: "../fltk_win64/include"
            cpp.libraryPaths: "../fltk_win64/lib"
            cpp.cxxFlags: "-Wno-unused-parameter"
            cpp.driverLinkerFlags: "-mwindows"
            cpp.staticLibraries: outer.concat([
                "fltk_png",
                "z",
                "fltk_jpeg",
                "ole32",
                "uuid",
                "comctl32",
            ])
        }

        Group {     // Properties for the produced executable
            fileTagsFilter: "application"
            qbs.install: true
        }
    }
}
